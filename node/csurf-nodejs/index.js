const express = require('express');
const csurf = require('csurf');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');

const isCookieEnabled =  csurf({ cookie: true });
const parser = bodyParser.urlencoded({extended:true});

const app = express();

const postRouter = express.Router();

app.set('view engine','ejs')
app.use(cookieParser());

app.use('/post', postRouter);

postRouter.get('/', (req, res) => {
    console.log(req.csrfToken())
})

app.get('/', isCookieEnabled,  function (req, res) {    
    res.render('login', { csrfToken: req.csrfToken() });
});

app.post('/process', parser, isCookieEnabled,  function (req, res) {
    res.send('successfully validated');
});

app.listen(3000, (err) => {
    if (err) console.log(err);
        console.log('Server is Running on port 3000');
});




